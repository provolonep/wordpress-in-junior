<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uTi7h5XEH6vVfsgmOJVa9UaeaqoZ5bOcryixPfu98KmDhipxB/JtF3Nna5YqPJqk4ebRcQQa/ba1deCrwBnwIA==');
define('SECURE_AUTH_KEY',  'WSbVt+q7JBoZVoPcUOzK8gKwCRQGigoypd+Kk84RRQIcVIG6bZQ2ueq/xIdnDVjciTq7PGMAMb5g0GyXahjg8Q==');
define('LOGGED_IN_KEY',    'lvJS/IvQeErsxZLT43UMryAYSMr8lRqUFxd8YAU35t5o3edigsUElO9x00HXbsWaEntSmZ2hW9pgYLSUCOrS3Q==');
define('NONCE_KEY',        'R/RvzZmvWDvqxjF4K5Xi3hWz3FS41BvHHxIzjDozE2Kcmj3HMhf2gLz5+oFE4x56CTVwbqSnrtusPJgOrFq9Kg==');
define('AUTH_SALT',        'w2FgLGRBbLs0+1AK3qTsC2gePCabCFk6DECOGcQw5EZcn6zDkZX/bRokwcEzD79YEkVB/RcyP1qWso/vqKiJFw==');
define('SECURE_AUTH_SALT', 'eFkEUM8RfdozEb+9pB4o8Y9OKFfLnyJi5tKkg+3jTCwlfKOCikKmUUB1uWYlvgqeQQ4TUkv2Rpie7u0phcZIMA==');
define('LOGGED_IN_SALT',   'rd01aQ5k5i1dDIZ6pIz4jrfmvBAS4dcCj5137texm9bYGmIZsqeYKGlAa7wg59+ZH1bYB46pZ1v9eePlabF2oA==');
define('NONCE_SALT',       'bpTt2IC9Br2ZiDvER6AVc7lNGewzpr9R088T5QWHgrmCKMxTXGaCTjCglUX2FewrrpDZBOufB82APYpfQpd5DA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
